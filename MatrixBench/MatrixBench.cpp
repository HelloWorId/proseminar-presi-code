#include "pch.h"
#include <iostream>
#include <chrono>
#include <random>
#include <vector>

class Mat {

public:

	Mat(int32_t nRows, int32_t nCols) {
		this->nRows = nRows;
		this->nCols = nCols;
		data.resize(nRows * nCols);
	}

	Mat(Mat&& m) {
		this->nRows = m.nRows;
		this->nCols = m.nCols;
		this->data = std::move(m.data);
	}

	void Print() const {
		for (int32_t i = 0; i < nRows; ++i) {
			for (int32_t j = 0; j < nCols; ++j) {
				std::cout << Get(i, j) << ' ';
			}

			std::cout << "\n";
		}
	}

	static Mat Rand(int32_t nRows, int32_t nCols) {
		Mat rm(nRows, nCols);
		std::default_random_engine re;
		std::uniform_real_distribution<double> dist(0.0, 1.0);

		for (int32_t i = 0; i < nRows; ++i) {
			for (int32_t j = 0; j < nCols; ++j) {
				rm.Set(i, j, dist(re));
			}
		}

		return rm;
	}

	__forceinline double Get(int32_t i, int32_t j) const {
		return data[i * nCols + j];
	}

	__forceinline void Set(int32_t i, int32_t j, double val) {
		data[i * nCols + j] = val;
	}

	__forceinline double EffSum() const {
		double sum = 0.0;
		for (int32_t i = 0; i < nRows; ++i) {
			for (int32_t j = 0; j < nCols; ++j) {
				sum += Get(i, j);
			}
		}
		return sum;
	}

	__forceinline double IneffSum() const {
		double sum = 0.0;
		for (int32_t j = 0; j < nCols; ++j) {
			for (int32_t i = 0; i < nRows; ++i) {
				sum += Get(i, j);
			}
		}
		return sum;
	}

private:
	int32_t nRows, nCols;
	std::vector<double> data;
};

void TestMatrix() {
	int32_t nRows, nCols, runs;
	bool rand;

	std::cout << "Num rows: ";
	std::cin >> nRows;
	std::cout << "Num columns: ";
	std::cin >> nCols;
	std::cout << "Num runs: ";
	std::cin >> runs;
	std::cout << "Randomize Matrix?: ";
	std::cin >> rand;

	std::cout << "Allocating..\n";

	std::unique_ptr<Mat> mat;
	if (rand) {
		mat = std::make_unique<Mat>(Mat::Rand(nRows, nCols));
	}
	else {
		mat = std::make_unique<Mat>(nRows, nCols);
	}

	double totalEff = 0.0;
	double totalIneff = 0.0;

	std::cout << "Starting matrix benchmark..\n";
	for (int32_t i = 0; i < runs; ++i) {
		auto startEff = std::chrono::high_resolution_clock::now();
		const double res1 = mat->EffSum();
		auto endEff = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> elapsedEff = endEff - startEff;

		auto startIneff = std::chrono::high_resolution_clock::now();
		const double res2 = mat->IneffSum();
		auto endIneff = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> elapsedIneff = endIneff - startIneff;

		totalEff += elapsedEff.count();
		totalIneff += elapsedIneff.count();
		std::cout << "Calculated " << res1 << " in " << elapsedEff.count() << '\n';
		std::cout << "Calculated " << res2 << " in " << elapsedIneff.count() << '\n';
	}

	std::cout << "------------------------\n";
	std::cout << "Matrix benchmark finished, dimensions " << nRows << " x " << nCols << '\n';
	std::cout << "Mean time row access: " << totalEff / runs << '\n';
	std::cout << "Mean time column access: " << totalIneff / runs << '\n';

	nRows *= 10;
	nCols *= 10;
	runs /= 10;
}

/*
Matrix benchmark finished, dimensions 30 x 30
Mean time row access: 1.0444e-06
Mean time column access: 8.957e-07

Matrix benchmark finished, dimensions 300 x 300
Mean time row access: 8.79487e-05
Mean time column access: 9.35356e-05

Matrix benchmark finished, dimensions 3000 x 3000
Mean time row access: 0.00876904
Mean time column access: 0.0445445

Matrix benchmark finished, dimensions 30000 x 30000
Mean time row access: 0.883595
Mean time column access: 9.26305
*/


int main(){
	TestMatrix();
	system("Pause");
}